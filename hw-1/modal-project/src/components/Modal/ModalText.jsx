import Modal from "./components/Modal"
import ModalHeader from "./components/ModalHeader";
import ModalWrapper from "./components/ModalWrapper";
import ModalFooter from "./components/ModalFooter";
import ModalBody from "./components/ModalBody";


const ModalText = ({isOpen, closeModal}) => {
    return (
        <ModalWrapper isOpen={isOpen} closeModal={closeModal}>
            <Modal closeModal={closeModal}>
                <ModalHeader>Add Product “NAME”</ModalHeader>
                <ModalBody>Description for you product</ModalBody>
                <ModalFooter firstText="Add to favorite" firstClick={closeModal}></ModalFooter>
            </Modal>
        </ModalWrapper>

    )
}
export default ModalText;