import PropTypes from 'prop-types';
import Button from '../../Button/Button';

const ModalClose = ({closeModal}) => {
    return(
        <button onClick={closeModal} type="button" className="modal-close">
            <img src="./image/close-cross.png"/>
        </button>
    )
}

ModalClose.propTypes = {
    click: PropTypes.func
}

export default ModalClose;