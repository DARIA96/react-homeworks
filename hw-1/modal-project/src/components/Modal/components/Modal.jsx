import ModalClose from './ModalClose'

const Modal = ({children, closeModal}) => {
    return (
        <div className="modal">
            <ModalClose closeModal={closeModal}></ModalClose>
            {children}
        </div>
        
    )
}

export default Modal;