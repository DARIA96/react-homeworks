const ModalImg = ({src, alt = "modal image"}) => {
    return (

        <div className="modal-image">
           <img src={src} alt={alt} />
        </div>
        
    )
}

export default ModalImg;