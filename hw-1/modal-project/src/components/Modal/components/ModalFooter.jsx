import Button from "../../Button/Button";

const ModalFooter = ({firstText, secondaryText, firstClick, secondaryClick}) => {
    return (
       <div className="modal-footer">
            {firstText && <Button classNames="button btn-img" onClick={firstClick}>{firstText}</Button>}
            {secondaryText && <Button classNames="button btn-img" onClick={secondaryClick}>{secondaryText}</Button>}
       </div>
    )
}

export default ModalFooter;