import PropTypes from 'prop-types'
import './Modal.scss'


const ModalWrapper = ({isOpen,children, closeModal}) =>{
    return (
        <>
           {isOpen && <div className="modal-wrapper" onClick={closeModal}>{children}</div>}     
        </>
    )
}

ModalWrapper.defaultProps = {
    isOpen: false
}

ModalWrapper.propTypes = {
    children: PropTypes.any,
    isOpen: PropTypes.bool
}
export default ModalWrapper;