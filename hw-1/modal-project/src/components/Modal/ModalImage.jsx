import Modal from "./components/Modal"
import ModalHeader from "./components/ModalHeader";
import ModalWrapper from "./components/ModalWrapper";
import ModalImg from "./components/ModalImg";
import ModalFooter from "./components/ModalFooter";
import ModalBody from "./components/ModalBody";


const ModalImage = ({isOpen, closeModal, src}) => {
    return (
        <ModalWrapper isOpen={isOpen} closeModal={closeModal}>
            <Modal closeModal={closeModal}>
                <ModalImg src={src}></ModalImg>
                <ModalHeader>Product Delete!</ModalHeader>
                <ModalBody>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</ModalBody>
                <ModalFooter firstText="no, cancel" firstClick={closeModal} secondaryText="yes, delete" secondaryClick={closeModal}></ModalFooter>
            </Modal>
        </ModalWrapper>

    )
}
export default ModalImage;