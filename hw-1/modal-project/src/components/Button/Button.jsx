import './Button.scss'
const Button = ({type="button", classNames="button", onClick, children}) => {
    return (    
            <button type={type} className={classNames} onClick={onClick}>{children}</button>
    )
}

export default Button