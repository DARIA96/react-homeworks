import { useState } from 'react'
import './App.scss'
import Button from './components/Button/Button'
import ModalImage from './components/Modal/ModalImage'
import ModalText from './components/Modal/ModalText'

function App() {
  const [isOpenModalImage, setIsOpenModalImage] = useState(false);
  const [isOpenModalText, setIsOpenModalText] = useState(false);

  const toggleModalImage = () => {
    setIsOpenModalImage(!isOpenModalImage);
  }
  const toggleModalText = () => {
    setIsOpenModalText(!isOpenModalText);
  }

  return (
    <>
    <div className='wrapper'>
      <Button classNames="button" onClick={toggleModalImage}>Open first modal</Button>
      <Button classNames="button" onClick={toggleModalText}>Open second modal</Button>
      <ModalImage src="./image/image.png" isOpen={isOpenModalImage} closeModal={toggleModalImage}></ModalImage>
      <ModalText isOpen={isOpenModalText} closeModal={toggleModalText}></ModalText>
    </div>
    </>
  )
}

export default App;
